'use strict';

module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.initConfig({
    jshint: {
      all: {
        options: {
          strict:    true,
          eqeqeq:    true,
          indent:    2,
          quotmark:  'single',
          undef:     true,
          unused:    true,
          esnext:    true,
          reporter:  require('jshint-stylish'),
          force:     true,
          node:      true
        },
        src: [
          'Gruntfile.js',
          'lib/**/*.js',
          'bin/**/*.js',
          'example/**/*.js'
        ]
      }
    }
  });

  grunt.registerTask('default', ['jshint:all']);
};

