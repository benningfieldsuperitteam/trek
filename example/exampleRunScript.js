'use strict';

module.exports = {
  /**
   * Run a script manually.
   * @param {Object} mssql - An mssql instance from the node-mssql package
   * (https://github.com/patriksimek/node-mssql).
   * @param {ConnectionPool} conn - A connected ConnectionPool instance
   * (https://github.com/patriksimek/node-mssql#connections-1).
   */
  run(mssql, conn) {
    const sql = `
      SELECT  w.widgetID, w.widgetName
      FROM    widgets w
      ORDER BY w.widgetName`;

    const req = new mssql.Request(conn);

    console.log(sql);

    return req.query(sql);
  }
};

