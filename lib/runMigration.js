'use strict';

module.exports = runMigration;

/**
 * Run a migration
 */
function runMigration(mssql, conn, migFile, direction) {
  const mig = require(migFile);

  if (!mig[direction]) {
    return Promise.reject(new Error(`"${direction}" method not defined in migration ${migFile}.`));
  }

  console.log('-------------------------------------------------------------');
  console.log(`Running migration ${direction} in file ${migFile}.`);
  console.log('-------------------------------------------------------------');

  return Promise.resolve(mig[direction](mssql, conn))
    .then(res => {
      const path = require('path');
      const req = new mssql.Request(conn);
      let   sql;

      console.log('Result: ', res);

      if (direction === 'up')
        sql = 'INSERT INTO trek_migrations (migrationName) VALUES (@migName)';
      else
        sql = 'DELETE FROM trek_migrations WHERE migrationName = @migName';

      req.input('migName', path.basename(migFile));

      return req.query(sql);
    });
}

