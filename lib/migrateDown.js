'use strict';

module.exports = migrateDown;

/**
 * Migrate down one migration.
 */
function migrateDown(mssql, conn, migDir) {
  const getLastMigration     = require('./getLastMigration');
  const getMigrationFileList = require('./getMigrationFileList');
  const runMigration         = require('./runMigration');
  const lastMigration        = getLastMigration(mssql, conn);
  const migrationFiles       = getMigrationFileList(migDir);

  return Promise
    .all([lastMigration, migrationFiles])
    .then(([lastMigration, migrationFiles]) => {
      if (lastMigration === null) {
        return Promise.resolve('No migration to bring down.');
      }

      if (migrationFiles.findIndex(file => file.indexOf(lastMigration) !== -1) === -1) {
        return Promise.reject(`Migration file ${lastMigration} not found.`);
      }

      return runMigration(mssql, conn, `${migDir}${lastMigration}`, 'down');
    });
}

