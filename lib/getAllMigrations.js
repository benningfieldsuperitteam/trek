'use strict';

module.exports = getAllMigrations;

/**
 * Get all migrations in the database.
 */
function getAllMigrations(mssql, conn) {
  const sql = `
    SELECT  migrationID, migrationName, runOn
    FROM    trek_migrations
    ORDER BY migrationName DESC`;
  const req = new mssql.Request(conn);

  return req
    .query(sql)
    .then(res => res.recordset);
}
