'use strict';

module.exports = promisify;

/**
 * Promisify a function.
 */
function promisify(func) {
  return function promiseCurry(...args) {
    return new Promise((resolve, reject) => {
      func(...args, (err, res) => {
        if (err)
          reject(err);
        resolve(res);
      });
    });
  };
}

