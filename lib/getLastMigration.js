'use strict';

module.exports = getLastMigration;

/**
 * Get the last migration in the database.
 */
function getLastMigration(mssql, conn) {
  const sql = `
    SELECT  TOP 1 migrationName
    FROM    trek_migrations
    ORDER BY migrationName DESC`;
  const req = new mssql.Request(conn);

  return req
    .query(sql)
    .then(res => res.recordset.length === 0 ? null : res.recordset[0].migrationName);
}

