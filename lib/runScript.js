'use strict';

module.exports = runScript;

/**
 * Run a script.
 */
function runScript(mssql, conn, scriptFile) {
  const script = require(scriptFile);

  if (!script.run)
    return Promise.reject(new Error(`"run" method not defined in script ${scriptFile}.`));

  console.log('-------------------------------------------------------------');
  console.log(`Invoking "run" in file ${scriptFile}.`);
  console.log('-------------------------------------------------------------');

  return Promise.resolve(script.run(mssql, conn))
    .then(res => console.log('Result: ', res));
}

