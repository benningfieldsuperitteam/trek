'use strict';

module.exports = migrateUp;

/**
 * Migrate up to the most recent migration.
 */
function migrateUp(mssql, conn, migDir) {
  const getAllMigrations     = require('./getAllMigrations');
  const getMigrationFileList = require('./getMigrationFileList');
  const runMigration         = require('./runMigration');
  const dbMigrations         = getAllMigrations(mssql, conn);
  const migrationFiles       = getMigrationFileList(migDir);

  return Promise
    .all([dbMigrations, migrationFiles])
    .then(([dbMigrations, migrationFiles]) => {
      // A list of migration files, path included, that don't exist in the db.
      return migrationFiles
        .filter(file => dbMigrations
          .findIndex(dbMig => dbMig.migrationName === file) === -1)
        .map(file => `${migDir}${file}`);
    })
    .then(missingMigs => {
      // Sequentially run each migration, and return a promise that resolves
      // with the result.  (See Promise Composition on MDN.)
      return missingMigs
        .reduce(
          (prom, mig) => prom
            .then(() => runMigration(mssql, conn, mig, 'up')),
          Promise.resolve());
    });
}

