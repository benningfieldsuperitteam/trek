'use strict';

module.exports = initMigrations;

/**
 * Initialize the migrations table if it does not already exist.
 */
function initMigrations(mssql, conn) {
  const sql = `
    IF NOT EXISTS (
      SELECT  name
      FROM    sys.tables
      WHERE   name = 'trek_migrations')
      CREATE TABLE [dbo].[trek_migrations] (
        migrationID INT NOT NULL PRIMARY KEY IDENTITY,
        runOn DATETIME NOT NULL DEFAULT GETDATE(),
        migrationName NVARCHAR(1000) NOT NULL
      )`;
  const req = new mssql.Request(conn);

  return req.query(sql);
}

