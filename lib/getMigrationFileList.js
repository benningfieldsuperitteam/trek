'use strict';

module.exports = getMigrationFileList;

/**
 * Get all the migration files in the migrations directory, ordered by name.
 */
function getMigrationFileList(migDir) {
  const fs        = require('fs');
  const promisify = require('./promisify');
  const readdir   = promisify(fs.readdir);

  return readdir(migDir)
    .then(files => files
      .sort()
      .filter(file => !!file.match(/^\d+.*\.js$/)));
}

