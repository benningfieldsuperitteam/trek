'use strict';

module.exports = createMigration;

/**
 * Create a migration.
 */
function createMigration(migDir, name) {
  const fs        = require('fs');
  const promisify = require('./promisify');
  const readFile  = promisify(fs.readFile);
  const writeFile = promisify(fs.writeFile);

  if (!name.match(/^\w+$/)) {
    return Promise.reject(new Error('Migrations names may only contain word characters (/^\w+$/).'));
  }
  else {
    const moment    = require('moment');
    const timestamp = moment().format('YYYY-MM-DD__HH-mm-ss-SSS');
    const fullPath  = `${migDir}${timestamp}__${name}.js`;

    console.log(`Creating migration: ${fullPath}`);

    return readFile(`${__dirname}/migrationTemplate.js`)
      .then(data => writeFile(fullPath, data));
  }
}

