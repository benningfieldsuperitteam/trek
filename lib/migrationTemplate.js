'use strict';

module.exports = {
  /**
   * Run the migration.
   * @param {Object} mssql - An mssql instance from the node-mssql package
   * (https://github.com/patriksimek/node-mssql).
   * @param {ConnectionPool} conn - A connected ConnectionPool instance
   * (https://github.com/patriksimek/node-mssql#connections-1).
   */
  up(mssql, conn) {
    const sql = ``;
    const req = new mssql.Request(conn);

    console.log(sql);

    return req.query(sql);
  },

  /**
   * Bring down a migration.
   * @param {Object} mssql - See up().
   * @param {ConnectionPool} conn - See up(). 
   */
  down(mssql, conn) {
    const sql = ``;
    const req = new mssql.Request(conn);

    console.log(sql);

    return req.query(sql);
  }
};

