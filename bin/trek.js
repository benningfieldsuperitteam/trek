#!/usr/bin/env node

'use strict';

const fs                 = require('fs');
const path               = require('path');
const mssql              = require('mssql');
const initMigrations     = require('../lib/initMigrations');
const migrateUp          = require('../lib/migrateUp');
const migrateDown        = require('../lib/migrateDown');
const runScript          = require('../lib/runScript');
const createMigration    = require('../lib/createMigration');
const baseDir            = `${process.cwd()}/`;
let   migDir             = `${baseDir}migrations/`;
let   settingsFile       = `${baseDir}connections.json`;
let   scriptFile;
let   command;
let   settings;
let   env;

const argv = require('yargs')
  .usage('Usage: $0 [<options>] <command>')

  .demandCommand(1, 1)
  .command('up', 'Run all new migrations.')
  .command('down', 'Undo the most recent migration.')
  .command('create <name>', 'Create a new migration.')
  .command('run <script-file>', 'Run a script.')

  .alias('c', 'connections-file')
  .nargs('c', 1)
  .default('c', settingsFile)
  .describe('c', 'Connections JSON file defining data sources.')

  .alias('m', 'migrations-dir')
  .nargs('m', 1)
  .default('m', migDir)
  .describe('m', 'Directory for migration scripts.')

  .argv;

// This is the command (up, down, create).
command = argv._[0];

if (command !== 'up' && command !== 'down' && command !== 'create' && command !== 'run') {
  console.error('Invalid command.');
  process.exit(1);
}

// Resolve settings file path.
if (path.isAbsolute(argv.c))
  settingsFile = path.resolve(argv.c);
else
  settingsFile = path.resolve(baseDir + argv.c);

if (!fs.existsSync(settingsFile)) {
  console.error(`Settings file (${settingsFile}) not found.`);
  process.exit(1);
}

// Resolve the migrations directory.
if (path.isAbsolute(argv.m))
  migDir = `${path.resolve(argv.m)}/`;
else
  migDir = `${path.resolve(baseDir + argv.m)}/`;

if (!fs.existsSync(migDir)) {
  console.log(`Migration directory (${migDir}) not found.  Creating it.`);
  fs.mkdirSync(migDir);
}

// Resolve the script file.
if (command === 'run') {
  if (path.isAbsolute(argv.scriptFile))
    scriptFile = argv.scriptFile;
  else
    scriptFile = `${path.resolve(baseDir + argv.scriptFile)}`;

  if (!fs.existsSync(scriptFile)) {
    console.error(`Script file (${scriptFile}) not found.`);
    process.exit(1);
  }
}

// Create a new migration if needed (no need to load the settings).
if (command === 'create') {
  createMigration(migDir, argv.name);
  return;
}

// This is the environment that's being used (e.g. dev, prod, uat).
env = process.env.NODE_ENV || 'dev';

// Read the connection settings for the environment.
settings = require(settingsFile);

if (!settings[env]) {
  console.error(`No connections found matching environment ${env}.`);
  process.exit(1);
}
else
  settings = settings[env];

// Process environment variables in settings.
settings.forEach(function(setting) {
  for (let key in setting) {
    if (setting[key] instanceof Object && setting[key].ENV) {
      if (process.env[setting[key].ENV] === undefined) {
        console.error(`Environmental variable "${setting[key].ENV}" not found.`);
        process.exit(1);
      }

      setting[key] = process.env[setting[key].ENV];
    }
  }
});

// Settings is an array of connection configuration objects for the environment
// (see https://github.com/patriksimek/node-mssql#general-same-for-all-drivers).
// The migrations are run against servers in sequential order (See Promise
// Composition on MDN).
settings
  .reduce((migrateRes, conOpts) =>
    migrateRes.then(() => migrate(conOpts)),
    Promise.resolve());

// Run the migration process against a single connection.
function migrate(conOpts) {
  let conn;

  console.log('-------------------------------------------------------------');
  console.log(`Server: ${conOpts.server} Database: ${conOpts.database}`);
  console.log('-------------------------------------------------------------');

  return new mssql.ConnectionPool(conOpts)
    .connect()
    // Store the connection for use below.
    .then(c => conn = c)
    // Initialize the migrations table.
    .then(() => initMigrations(mssql, conn))
    .then(() => {
      switch (command) {
        case 'up':
          return migrateUp(mssql, conn, migDir);
        case 'down':
          return migrateDown(mssql, conn, migDir);
        case 'run':
          return runScript(mssql, conn, scriptFile);
      }
    })
    .then(() => conn.close())
    // Report any errors.
    .catch(err => {
      console.error('-------------------------------------------------------------');
      console.error(`Error on Server: ${conOpts.server} Database: ${conOpts.database}`);
      console.error(err);
      console.error('-------------------------------------------------------------');

      if (conn) {
        conn.close();
      }

      process.exit(1);
    });
}

